<?php
$basedir = realpath(dirname(dirname(__FILE__)));

require_once $basedir . '/library/Jariah/Application.php';

$application = new Jariah_Application($basedir, 'development');
$application->bootstrap()
        ->run();