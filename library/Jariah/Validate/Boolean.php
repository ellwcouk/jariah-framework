<?php

class Jariah_Validate_Boolean implements Jariah_Validate_Interface
{
    
    protected $_error;
    
    protected $_args;
    
    protected $_value;
    
    public function isValid($value, $args = array())
    {
        $this->_value = $value;
        $this->_args = $args;
        
        if (!is_string($value) && !is_int($value) && !is_float($value))
        {
            $this->_error = $value . ' is not a string, int or float';
            return false;
        }
        
        if($value == '')
        {
            $this->_error = $value . ' is empty';
            return false;
        }
        
        $true = array(
            'true' => 'bool',
            'yes' => 'bool',
            '1' => 'bool',
            'y' => 'bool'
        );
        
        if(!isset($true[$value]))
        {
            $this->_error = $value . ' does not appear to be boolean';
            return false;
        }
        
        return true;
    }
    
    public function getErrors()
    {
        return $this->_errors;
    }
    
    public function getArgs()
    {
        return $this->_args;
    }
    
    public function getValue()
    {
        return $this->_value;
    }
}