<?php

class Jariah_Validate_Numerical implements Jariah_Validate_Interface
{
    
    protected $_error;
    
    protected $_args;
    
    protected $_value;
    
    public function isValid($value, $args = array())
    {
        $this->_value = $value;
        $this->_args = $args;
        
        if (!is_string($value) && !is_int($value) && !is_float($value))
        {
            $this->_error = $value . ' is not a string, int or float';
            return false;
        }
        
        if($value == '')
        {
            $this->_error = $value . ' is empty';
            return false;
        }
        
        $numerical = new Jariah_Filter_Numerical();
        
        if(($numerical->filter($value)) != $value)
        {
            $this->_error = $value . ' contains non-numerical characters';
            return false;
        }
        
        return true;
    }
    
    public function getErrors()
    {
        return $this->_errors;
    }
    
    public function getArgs()
    {
        return $this->_args;
    }
    
    public function getValue()
    {
        return $this->_value;
    }
}