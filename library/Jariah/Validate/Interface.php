<?php

interface Jariah_Validate_Interface
{
    
    public function isValid($value, $args);
    
    public function getErrors();
    
    public function getArgs();
    
    public function getValue();
}