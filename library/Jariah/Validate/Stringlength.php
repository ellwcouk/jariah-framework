<?php

class Jariah_Validate_Stringlength implements Jariah_Validate_Interface
{
    
    protected $_error;
    
    protected $_args;
    
    protected $_value;
    
    public function isValid($value, $args = array('min' => 0, 'max' => null))
    {
        $this->_value = $value;
        $this->_args = $args;
        
        if (!is_string($value))
        {
            $this->_error = $value . ' is not a string, int or float';
            return false;
        }
        
        if($value == '')
        {
            $this->_error = $value . ' is empty';
            return false;
        }
        
        $length = iconv_strlen($value);
        
        if($length < $args['min'])
        {
            $this->_error = $value . ' is less than ' . $args['min'] . ' characters';
            return false;
        }
        
        if($args['max'] !== null && $length > $args['max'])
        {
            $this->_error = $value . ' is more than ' . $args['max'] . ' characters';
            return false;
        }
        
        return true;
    }
    
    public function getErrors()
    {
        return $this->_errors;
    }
    
    public function getArgs()
    {
        return $this->_args;
    }
    
    public function getValue()
    {
        return $this->_value;
    }
}