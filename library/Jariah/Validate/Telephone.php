<?php

class Jariah_Validate_Numerical implements Jariah_Validate_Interface
{
    
    protected $_error;
    
    protected $_args;
    
    protected $_value;
    
    public function isValid($value, $args = array())
    {
        $this->_value = $value;
        $this->_args = $args;
        
        if (!is_string($value) && !is_int($value) && !is_float($value))
        {
            $this->_error = $value . ' is not a string, int or float';
            return false;
        }
        
        if($value == '')
        {
            $this->_error = $value . ' is empty';
            return false;
        }
        
        if( !filter_var($value, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => "/^0[1-9][0-9]{8,9}$/"))))
        {
            $this->_error = $value . ' does not seem to be a UK telephone number';
            return false;
        }
        
        return true;
    }
    
    public function getErrors()
    {
        return $this->_errors;
    }
    
    public function getArgs()
    {
        return $this->_args;
    }
    
    public function getValue()
    {
        return $this->_value;
    }
}