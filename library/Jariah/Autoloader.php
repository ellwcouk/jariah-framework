<?php
/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah_Autoloader
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

class Jariah_Autoloader
{
    
    public static $jAinstance = null;
    
    public static function getInstance()
    {
        if( self::$jAinstance === null )
        {
            self::$jAinstance = new Jariah_Autoloader();
        }
        
        return self::$jAinstance;
    }
    
    public function loadClass($class)
    {
        if( class_exists($class, false) || interface_exists($class, false) )
        {
            return;
        }
        
        $fileLocation = str_replace('_', '/', $class);
        if( @file_get_contents($fileLocation . '.php', true, null, 0, 1) )
        {
            include $fileLocation . '.php';
            return;
        }
    }
    
    public function __construct()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }
}