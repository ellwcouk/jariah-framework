<?php
/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah_Registry
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

class Jariah_Registry
{
    
    public static $jRinstance = null;
    
    protected static $_registry = array();
    
    public static function getInstance()
    {
        if( self::$jRinstance === null )
        {
            self::$jRinstance = new Jariah_Registry();
        }
        
        return self::$jRinstance;
    }
    
    public static function set($key, $value)
    {
        self::$_registry[$key] = $value;
    }
    
    public static function get($key)
    {
        return self::$_registry[$key];
    }
}