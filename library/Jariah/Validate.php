<?php

class Jariah_Validate
{
    
    public static function validate($validationMethod, $value, $args = array())
    {
        $validationMethod = 'Jariah_Validate_' . ucfirst($validationMethod);
        
        if(!Jariah_Application_Controller::isDispatchable($validationMethod, 'validate'))
        {
            throw new Exception('Validation method not found ' . $validationMethod);
        }
        
        $validate = new $validationMethod;
        return $validate->validate($value, $args);
    }
}