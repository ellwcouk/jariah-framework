<?php

class Jariah_Filter
{
    
    public static function filter($filter, $value, $args = array())
    {
        $filter = (string)'Jariah_Filter_' . ucfirst($filter);
        
        if(!Jariah_Application_Controller::isDispatchable($filter, 'filter'))
        {
            throw new Exception('Filter not found ' . $filter);
        }
        
        $filter = new $filter;
        return $filter->filter($value, $args);
    }
}