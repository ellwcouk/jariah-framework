<?php
/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah_Controller
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

class Jariah_Controller
{
    
    public function isAjaxRequest()
    {
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            return true;
        }
        
        return false;
    }
    
    public function isMobileBrowser()
    {
        $mobile = new Jariah_Mobile();
        if($mobile->isMobile())
        {
            return true;
        }
        
        return false;
    }
    
    public function disableLayout()
    {
        Jariah_Layout::getInstance()->disableLayout();
    }
    
    public function switchLayout($layoutName)
    {
        Jariah_layout::getInstance()->setLayoutName($layoutName);
    }
    
    public function filter($filter, $value, $args = array())
    {
        return Jariah_Filter::filter($filter, $value, $args);
    }
    
    public function validate($validationMethod, $value, $args = array())
    {
        return Jariah_Validate::validate($validationMethod, $value, $args);
    }
}