<?php

/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah_Layout
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

class Jariah_View extends Jariah_Layout
{
    protected $_viewName;
    protected $_controllerName;
    protected $_view;
    
    public function setView($view = null)
    {
        if($view != null)
        {
            $this->_view = $view;
            return $this;
        }
        
        $uri = Jariah_Registry::get('url');
        
        if(@file_get_contents('views/' . $this->_controllerName . '/' . $this->_viewName . '.php',
                true, null, 0, 1))
        {
            ob_start();
            include 'views/' . $this->_controllerName . '/' . $this->_viewName . '.php';
            $this->_view = ob_get_contents();
            ob_end_clean();
        } elseif(@file_get_contents('views/' . $uri['controller'] . '/' . $uri['action'] . '.php',
                true, null, 0, 1))
        {
            ob_start();
            include 'views/' . $uri['controller'] . '/' . $uri['action'] . '.php';
            $this->_view = ob_get_contents();
            ob_end_clean();
        } else {
            $errorDetails = array(
                '_controllerName' => $this->_controllerName,
                '_viewName' => $this->_viewName,
                'uri[controller]' => $uri['controller'],
                'uri[action]' => $uri['action']
            );
            throw new Exception('Could not locate view: ' . print_r($errorDetails));
        }
        
        Jariah_Layout::getInstance()->setContent($this->_view);
        return $this;
    }
    
    public function getViewName()
    {
        return $this->_viewName;
    }
    
    public function setViewName($viewName)
    {
        $this->_viewName = $viewName;
        return $this;
    }
    
    public function getView()
    {
        return $this->_view;
    }
    
    public function setControllerAndActionNames($controllerName, $actionName)
    {
        $this->_controllerName = $controllerName;
        $this->_viewName = $actionName;
        return $this;
    }
   
}