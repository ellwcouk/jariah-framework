<?php
/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah_Application_Bootstrap
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

class Jariah_Application_Bootstrap
{
    protected $_application;
    
    public function __construct( Jariah_Application $application )
    {
        $this->_application = $application;
    }
    
    public function bootstrap()
    {
        $bootstrap = new Application_Bootstrap();
        $bootstrapMethods = get_class_methods($bootstrap);
        
        foreach( $bootstrapMethods as $methodName )
        {
            if( mb_substr($methodName, 0, 5) == '_init' )
            {
                $bootstrap->$methodName();
            }
        }
    }
    
}