<?php

class Jariah_Application_phpSettings
{
    
    /**
     * Retrieve a PHP setting from php.ini
     * @param string $phpSetting The name of the setting
     * @return string | null
     */
    public static function getPhpSetting($phpSetting)
    {
        $setting = ini_get($phpSetting);
        if( $setting == '' || $setting == null )
        {
            return false;
        }
        
        return $setting;
    }
    
    /**
     * Set some PHP settings
     * @param array $phpSettings 
     * @return Jariah_Application
     */
    public static function setPhpSettings( array $settings, $prefix = '')
    {
        foreach ($settings as $key => $value) {
            $key = empty($prefix) ? $key : $prefix . $key;
            if (is_scalar($value)) {
                ini_set($key, $value);
            } elseif (is_array($value)) {
                $this->setPhpSettings($value, $key . '.');
            }
        }

        return $this;
    }
}