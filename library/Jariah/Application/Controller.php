<?php
/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah_Application_Controller
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

class Jariah_Application_Controller
{
    public static function isDispatchable($controllerName, $actionName)
    {
        if(!class_exists($controllerName, true))
        {
            return false;
        }
        $class = new $controllerName;
        if(!method_exists($class, $actionName))
        {
            return false;
        }
        unset($class);
        return true;
    }
    
}