<?php
/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah_Application_Bootstrap_Bootstrap
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

/**
 * Contains bootstrap methods 
 */
class Jariah_Application_Bootstrap_Bootstrap
{
    
    public function setPhpSetting($name, $value)
    {
        Jariah_Application_phpSettings::setPhpSettings(array($name => $value));
    }
}