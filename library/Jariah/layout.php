<?php

/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah_Layout
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

class Jariah_Layout
{
    /*
     * The Jariah_Layout object
     * @var object
     */
    public static $jLinstance = null;
    
    /**
     * The layout name to use on the include_path
     * @var string
     */
    protected $_layoutName = 'default';
    
    /**
     * The file contents of the layout
     * @var string
     */
    protected $_layout;
    
    protected $_content;
    
    /**
     * Return or create Jariah_Layout object
     * @return object Jariah_Layout
     */
    public static function getInstance()
    {
        if(!self::$jLinstance)
        {
            self::$jLinstance = new Jariah_Layout();
        }
        
        return self::$jLinstance;
    }
    
    /**
     * Set the layout name
     * @param string $layoutName
     * @return object Jariah_Layout
     */
    public function setLayoutName($layoutName)
    {
        $this->_layoutName = $layoutName;
        return self::$jLinstance;
    }
    
    /**
     * Returns the layout name
     * @return string
     */
    public function getLayoutName()
    {
        return $this->_layoutName;
    }
    
    /**
     * sets the view script file (html, PHP etc)
     * @param string $view
     * @return object Jariah_Layout
     */
    public function setViewScript($view)
    {
        $this->_view = $view;
        return self::$jLinstance;
    }
    
    /**
     * Returns the base Url and the url supplied
     * @param string $url
     * @return string
     */
    public function baseUrl($url = null)
    {
        return substr($_SERVER['PHP_SELF'], 0, -9) . $url;
    }
    
    /**
     * Sets the layout file (html, PHP etc)
     * @param string $layout
     * @return object Jariah_Layout 
     */
    public function setLayout($layout)
    {
        $this->_layout = $layout;
        return self::$jLinstance;
    }
    
    /**
     * Returns the layout script
     * @return string
     */
    public function getLayout()
    {
        return $this->_layout;
    }
    
    /**
     * Disable the layout
     * @return object Jariah_Layout 
     */
    public function disableLayout()
    {
        $this->setLayoutName('disabled');
        return self::$jLinstance;
    }
    
    public function setContent($viewScript)
    {
        $this->_content = $viewScript;
        return self::$jLinstance;
    }
    
    public function content()
    {
        return $this->_content;
    }
    
    /**
     * Do the layout and return it...
     * @return string
     */
    public function run()
    {
        if($this->getLayoutName() === null)
        {
            $this->setLayoutName($this->getLayoutName());
        }
        
        $checkMobile = Jariah_Registry::get('mobile_detect');
        $checkAjax = Jariah_Registry::get('ajax_detect');
        
        if($checkMobile == '1' && $this->_isMobile())
        {
            $this->setLayoutName('default.mobile.php');
        }
        
        if( $checkAjax == '1' && $this->_isAjax())
        {
            $this->disableLayout();
        }
        
        $this->_applyLayout();

        return $this->getLayout();
    }
    
    /**
     * Apply the layout
     * @return Object Jariah_Layout
     * @throws Exception 
     */
    protected function _applyLayout()
    {
        if($this->getLayoutName() === null)
        {
            throw new Exception('Layout name is null');
        }
        
        if(@file_get_contents('layouts/' . $this->getLayoutName() . '.php', true, null, 0, 1))
        {
            ob_start();
            include 'layouts/' . $this->getLayoutName() . '.php';
            $this->setLayout(ob_get_contents());
            ob_end_clean();
        } else {
            throw new Exception('Could not apply layout ' . $this->getLayoutName());
        }
        
        return $this;
    }
    
    /**
     * Detects if it is an AJAX request.
     * @return bool
     */
    protected function _isAjax()
    {
        $controller = new Jariah_Controller();
        return $controller->isAjaxRequest();
    }
    
    /**
     * Detects a mobile browser
     * @return bool
     */
    protected function _isMobile()
    {
        $controller = new Jariah_Controller();
        return $controller->isMobileBrowser();
    }
}