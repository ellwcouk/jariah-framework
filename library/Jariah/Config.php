<?php
/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah_Config
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

class Jariah_Config
{
    
    public static function setConfig($configLocation)
    {
        if(!is_string($configLocation))
        {
            throw new Exception('Configuation file path must be a string');
        }
              
        if(@file_get_contents($configLocation, true, null, 0, 1))
        {
            include $configLocation;
        } else {
            throw new Exception('Configuation file not found');
        }
        
        foreach($config as $key => $value)
        {
            Jariah_Registry::set($key, $value);
        }
    }
}