<?php

class Jariah_Filter_Alphanumerical implements Jariah_Filter_Interface
{
    protected $_oldValue;
    
    protected $_newValue;
    
    protected $_args;
    
    /**
     * Filters out all but a to z and 0 to 9 (case insensitive). Allows one space.
     * Changed the regex from: #\W#  to /[^A-Za-z0-9 ]/ because it fails when using
     * UTF-8 characters.
     * @param string $value
     * @param array $args
     * @return string 
     */
    public function filter($value, $args = array())
    {
        $this->_oldValue = $value;
        $this->_args = $args;
        $this->_newValue =  preg_replace('/[^A-Za-z0-9 ]/', '', $value);
        
        return $this->_newValue;
    }
    
    public function getOldValue()
    {
        return $this->_oldValue;
    }
    
    public function getNewValue()
    {
        return $this->_newValue;
    }
    
    public function getArgs()
    {
        return $this->_args;
    }
}