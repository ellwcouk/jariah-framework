<?php

interface Jariah_Filter_Interface
{
    
    public function filter($value, $args = array());
    
    public function getOldValue();
    
    public function getNewValue();
    
    public function getArgs();
}