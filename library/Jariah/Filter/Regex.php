<?php

class Jariah_Filter_Regex implements Jariah_Filter_Interface
{
    
    protected $_oldValue;
    
    protected $_newValue;
    
    protected $_args;
    
    /**
     * Filters out according to user defined regex
     * @param string $value
     * @param array $args
     * @return string 
     */
    public function filter($value, $args = array('regex' => '', 'replacement' => ''))
    {        
        if(!isset($args['regex']) || !isset($args['replacement'])
                || $args['regex'] == '')
        {
            throw new Exception('No regex or replacement found. Please pass the 
                regex in an array format as 
                array("regex" => "YOUR_REGEX", "replacement" => "YOUR_REPLACEMENT")');
        }

        
        if(!Jariah_Helpers::isRegex($args['regex'], $value))
        {
            throw new Exception('Doesn\'t seem to be a valid regex');
        }
        
        $this->_oldValue = $value;
        $this->_args = $args;
        
        $this->_newValue = preg_replace($args['regex'], $args['replacement'], $value);
        return $this->_newValue;
    }
    
    public function getOldValue()
    {
        return $this->_oldValue;
    }
    
    public function getNewValue()
    {
        return $this->_newValue;
    }
    
    public function getArgs()
    {
        return $this->_args;
    }
}