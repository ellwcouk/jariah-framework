<?php

class Jariah_Filter_Stringtoupper implements Jariah_Filter_Interface
{
    protected $_oldValue;
    
    protected $_newValue;
    
    protected $_args;
    
    public function filter($value, $args = array())
    {
        $this->_oldValue = $value;
        $this->_args = $args;
        $this->_newValue = preg_replace('/[\s\h\v]/u', '', $value);
        
        return $this->_newValue;
    }
    
    public function getOldValue()
    {
        return $this->_oldValue;
    }
    
    public function getNewValue()
    {
        return $this->_newValue;
    }
    
    public function getArgs()
    {
        return $this->_args;
    }
}