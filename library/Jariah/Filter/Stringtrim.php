<?php


/**
 * Taken from Zend_Filter_StringTrim 
 */
class Jariah_Filter_Stringtrim implements Jariah_Filter_Interface
{
    protected $_oldValue;
    
    protected $_newValue;
    
    protected $_args;
    
    /**
     * Trims a string. Accepts charlist is an argument
     * @param string $value
     * @param array $args
     * @return string 
     */
    public function filter($value, $args = array())
    {
        $this->_oldValue = $value;
        $this->_args = $args;
        
        if(!isset($args['charlist']))
        {
            $this->_newValue = $this->_unicodeTrim($value);
        } else {
            $this->_newValue = $this->_unicodeTrim($value, $args['charlist']);
        }
        
        return $this->_newValue;
    }
    
    public function getOldValue()
    {
        return $this->_oldValue;
    }
    
    public function getNewValue()
    {
        return $this->_newValue;
    }
    
    public function getArgs()
    {
        return $this->_args;
    }
    
    protected function _unicodeTrim($value, $charlist = '\\\\s')
    {
        $chars = preg_replace(
            array( '/[\^\-\]\\\]/S', '/\\\{4}/S', '/\//'),
            array( '\\\\\\0', '\\', '\/' ),
            $charlist
        );
        
        $pattern = '^[' . $chars . ']*|[' . $chars . ']*$';
        return preg_replace("/$pattern/sSD", '', $value);
    }
}