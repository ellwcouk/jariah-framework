<?php

class Jariah_Helpers
{
    
    /**
     * Function to test if the regex is valid against a value
     * @param string $regex
     * @param string $value
     * @return boolean 
     */
    public static function isRegex($regex, $valueToTestAgainst)
    {
        if( @preg_match($regex, $valueToTestAgainst) === false )
        {
            return false;
        } else {
            return true;
        }
    }
}