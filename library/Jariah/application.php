<?php
/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah_Application
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

class Jariah_Application
{
    
    /**
     * Autoloader Object
     * @var object 
     */
    protected $_autoloader;
    
    /**
     * Application Environment
     * @var string
     */
    protected $_environment;
    
    /**
     * Base directory (containing library, application etc)
     * @var string 
     */
    protected $_baseDir;
    
    /**
     * Setup the Application ready for bootstrapping... 
     * @return object Jariah_Application
     */
    public function __construct($baseDir, $environment)
    {
        $this->_baseDir = $baseDir;
        $this->setEnvironment($environment);
        
        $this->setIncludePaths(array(
            $this->_baseDir . '/library',
            $this->_baseDir . '/application',
            $this->_baseDir
        ));
        
        require_once 'Jariah/Autoloader.php';
        $this->_autoloader = Jariah_Autoloader::getInstance();
        
        return $this;
    }
    
    /**
     * Prepare the application
     * @return object Jariah_Application
     */
    public function bootstrap()
    {
        $config = new Jariah_Config();
        $config->setConfig($this->_baseDir . '/application/configs/config.php');
        
        $bootstrap = new Jariah_Application_Bootstrap($this);
        $bootstrap->bootstrap();
        
        return $this;
    }
    
    /**
     * Retrieve autoloader instance
     * @return object Jariah_Autoloader
     */
    public function getAutoloader()
    {
        return $this->_autoloader;
    }
    
    /**
     * Retrieve current environment
     * @return string
     */
    public function getEnvironment()
    {
        return $this->_environment;
    }
    
    /**
     * Retrieve include paths
     * @return string
     */
    public function getIncludePaths()
    {
        return get_include_path();
    }
    
    
    
    /**
     * Removes include paths from php's include_path
     * @param array $includePaths 
     * @return object Jariah_Application
     */
    public function removeIncludePaths( array $includePaths)
    {
        $paths = explode(PATH_SEPARATOR, get_include_path());
	$i = 0;
	foreach($includePaths as $key => $path)
	{
            if(in_array($path, $paths, false))
            {
                $newKey = array_search($path, $paths, false);
                unset($paths[$newKey]);
            }
            $i++;
	}
		
	$i = 0;
	foreach($paths as $key => $path)
	{
            if($i == 0)
            {
                set_include_path($path);	
            } else {
		set_include_path(get_include_path() . PATH_SEPARATOR . $path);
            }
            $i++;
	}
		
	return $this;
    }
    
    /**
     * Runs the application 
     * @return string
     */
    public function run()
    {
        $uri = new Jariah_Url();
        $url = $uri->getUrl();
        Jariah_Registry::set('url', $url);
        
        $controllerClassName = 'Application_Controllers_' . $url['controller'] . 'Controller';
        $actionName = $url['action'] . 'Action';
        $controller = $url['controller'];
        $action = $url['action'];
        
        $isDispatchable = Jariah_Application_Controller::isDispatchable($controllerClassName, $actionName);
        if(!$isDispatchable)
        {
            $controllerClassName = 'Application_Controllers_errorController';
            $controller = 'error';
            $actionName = 'errorAction';
            $action = 'error';
        }
        
        $class = new $controllerClassName;
        $class->init();
        $class->$actionName();
        
        $layout = Jariah_Layout::getInstance();
        $view = new Jariah_View();
        $view->setControllerAndActionNames($controller, $action);
        $view->setView();
        echo $layout->run();
    }
    
    /**
     * Set the autoloader object
     * @param object $autoloaderObject
     * @return object Jariah_Application
     */
    public function setAutoloader($autoloaderObject)
    {
        $this->_autoloader = $autoloaderObject;
        return $this;
    }
    
    /**
     * Set the environment
     * @param string $environment 
     * @return object Jariah_Application
     */
    public function setEnvironment($environment)
    {
        $this->_environment = $environment;
        return $this;
    }
    
    /**
     * Add include paths to PHP's include_path
     * @param array $includePaths 
     * @return object Jariah_Application
     */
    public function setIncludePaths( array $includePaths)
    {
        $paths = explode(PATH_SEPARATOR, get_include_path());
        foreach($includePaths as $key => $path)
        {
            if(!in_array($path, $paths, false))
            {
                set_include_path(get_include_path() . PATH_SEPARATOR . $path);	
            }
	}
		
	return $this;
    }
    
}