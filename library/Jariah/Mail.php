<?php

/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package Jariah_Mail
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

class Jariah_Mail
{
    /**
     * Holds the config array of username, password and auth
     * @var array
     */
    protected $_config = array();
    
    /**
     * Holds the URL for the mail server
     * @var string
     */
    protected $_mailServer;
    
    /**
     * The object for the transport
     * @var object
     */
    protected $_transport;
    
    /**
     * Sets up the transport ready for mail.
     * @param string $mailServer
     * @param string $username
     * @param string $password
     * @param string $auth
     * @return Object Jariah_Mail 
     */
    public function setTransport($mailServer, $username = null, $password = null, $auth = 'login')
    {        
        if($username != null && $password != null)
        {
            $this->_config['username'] = $username;
            $this->_config['password'] = $password;
            $this->_config['auth'] = $auth;;
        }
        
        $this->_mailServer = $mailServer;
        
        $this->_transport = new Zend_Mail_Transport_Smtp($transport, $this->_config);
        
        return $this;
    }
    
    
    /**
     * Sends an HTML email
     * @param string $body
     * @param string $toEmail
     * @param string $fromEmail
     * @param string $toName
     * @param string $fromName
     * @param string $subject
     * @return object Jariah_Mail 
     */
    public function sendHtmlMail($body, $toEmail, $fromEmail, $toName = null, $fromName = null, $subject = null)
    {
        $mail = new Zend_Mail();
        $mail->setBodyHtml($body);
        $mail->setFrom($fromEmail, $fromName);
        $mail->addTo($toEmail, $toName);
        $mail->setSubject($subject);
        
        if($this->_transport != null)
        {
            $mail->send($this->_transport);
        } else {
            $mail->send();
        }
        
        return $this;
    }
    
    /**
     * Sends an email
     * @param string $body
     * @param string $toEmail
     * @param string $fromEmail
     * @param string $toName
     * @param string $fromName
     * @param string $subject
     * @return object Jariah_Mail 
     */
    public function sendMail($body, $toEmail, $fromEmail, $toName = null, $fromName = null, $subject = null)
    {
        $mail = new Zend_Mail();
        $mail->setBodyText($body);
        $mail->setFrom($fromEmail, $fromName);
        $mail->addTo($toEmail, $toName);
        $mail->setSubject($subject);
        
        if($this->_transport != null)
        {
            $mail->send($this->_transport);
        } else {
            $mail->send();
        }
        
        return $this;
    }
}