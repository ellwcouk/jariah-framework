<?php

/**
 * Allowed URL Character
 * 
 * This specifies which characters, with a regular expression are permitted in the URL
 * Do not change this unless you are nuts! 
 */
$config['permitted_url_chars'] = '/[^0-9a-zA-Z\/]/';

/**
 * Allow variables in the URL?
 * e.g. c=controller&a=action&var1=value1 
 */
$config['use_get_vars'] = false;