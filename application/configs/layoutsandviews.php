<?php

/**
 * Detect AJAX
 * Detect an AJAX call and automatically disable the layout? 
 */
$config['ajax_detect'] = true;

/**
 * Detect mobile and switch to mobile layout? 
 */
$config['mobile_detect'] = true;