<?php
/**
 * Jariah Framework
 * 
 * @category Jariah
 * @package config
 * @copyright Copyright (c) 2012 James Elliott
 * @license  http://creativecommons.org/licenses/by-sa/3.0/
 * @version 0.2
 */

/**
 * The configuration file is split into smaller files to make it easier to manage
 * and find the variable you want to change without searching one, big file. 
 * Do not change this file unless you are adding a config file.
 */

/**
 * Incude the files 
 */
$configDir = realpath(dirname(__FILE__));

include $configDir . '/system.php';
include $configDir . '/mysql.php';
include $configDir . '/layoutsAndViews.php';