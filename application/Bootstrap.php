<?php

class Application_Bootstrap extends Jariah_Application_Bootstrap_Bootstrap
{
    
    protected function _initPhpSettings()
    {
        $this->setPhpSetting('date.timezone', 'Europe/London');
    }
}